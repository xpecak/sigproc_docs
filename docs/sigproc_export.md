# Sigproc Export user guide

The tool can be used for basic data analysis and export of the data to a .CSV file.

![Sigproc Exporter GUI](img/sigproc_exporter.png)

## Graph

The graph shows the measured data.
You can control the graph by panning with left-click and drag, and scrolling to zoom.
If you scroll or pan on the axis, you only pan/zoom the one axis.
You can access more graph settings to tweak by rightclicking the graph, or the axes.
To export the image of the graph, you can rightclick the graph and select __Export__.

## Tabs

The bottom part of the app controls the data shown in the graph.
There are several tabs related to different measurements.

### General
Currently there is only the option to open a file for analysis and export.
When you open a file, only the tabs related to the measurements in the current file are active.

### Raw data
This tab allows graphing and exporting the raw measured data.
Raw measurements are shown in the list on the left, sorted by their start time.
Clicking an item in the list selects that item and plots it into the graph.
The tab also shows the start and stop times of the measurement and also the sample count.
The selected data can be exported using the __Export to CSV__ button.

The file is exported to a .CSV format, which is a text based, human readable format.
Most common data analysis tools can open this file (Matlab, Python, Excel etc.)

The general format is:

```
M samples chronologicaly
M samples chronologicaly
.....
.....
```

Where M is the __Export Width__ value above the export button.

### Continuous data
This tab allows graphing and exporting continuous data measurements.
Continuous measurements are shown in the list on the left side, sorted by their start time.
Clicking an item selects that measurement and plots it into the graph.
The tab also shows the start and stop times of the measurement and also the sample count.
You can select which measurement is plotted by un/checking the check boxes.
The __Export to CSV__ button exports the file to a .CSV file.

The format is:

```
RMS, Count1, Count2
{value}, {value}, {value}
{value}, {value}m {value}
```

### Discontinuous data
This tab allows graphing and exporting discontinuous data measurements.
Detected events are shown in the list on the left side, sorted by their detection time.
Clicking an item selects that event.
When selected, the sampled event data is plotted and several other parameters are shown in the tab.
You can plot additional parameters by checking the according checkboxes.
The __Export data__ button exports the raw data of the event to a CSV file.

### Trends
This tab allows graphing and export of trends of parameters of discontinuous events in time.
When clicked, it might take a while to load and the program might seem to be stuck, as it has to calculate parameters for all events first.
You can select which parameters to plot by checking the according checkboxes.

The __Export data__ button exports the trend data of the events to a CSV file.