# Developer guide

## HDF5 data format

We use HDF5 format to save the data.
HDF5 is a widely used format for large dataset, being capable of storing up to terabytes of data if the filesystem permits.
In our implementation, we use h5py python library to read/write the files.
Similar libraries can be found for any language, including MATLAB, C and C++. etc.
There are also HDF file viewers available for viewing the raw files, for example [HDFView](https://www.hdfgroup.org/downloads/hdfview/).

The HDF5 file currentcly contains 3 groups:
- `/raw/` - raw measurement values
- `/continuous/` - data from continuous measurements
- `/events/` - data from discontinuous measurements

## Raw data
Raw data saved directly from the digitizer.
Each measurement is a separate dataset named by the date and time of the start of the measurement.
They are saved in chunks of 4096 samples.
Ie, the first 4096 samples are indexed `{dataset index}[0][0.....4095]`, the next 4096 are indexed `{dataset_index}[1][0.....4095]` and so on.
There are also two attributes for each dataset:
- `start` - UNIX timestamp of start of the measurement
- `stop` - UNIX timestamp of the end of the measurement

## Continuous data
Each separate continuous measurement is separated dataset named by the date and time of the start of the measurement.
They are saved as a 2D array with 3 columns.
Column 0 is calculated RMS value, column 1 is number of events above Count1 value and column 2 is number of events above Count2.
There are also two attributes for each dataset:
- `start` - UNIX timestamp of start of the measurement
- `stop` - UNIX timestamp of the end of the measurement

## Discontinuous data
Each detected event is saved in separate dataset.
The dataset is name `event-{timestamp}`, where `{timestamp}` is a date and time of the event start.
The dataset is the raw detected samples from the event.
Each dataset has following attributes:
- `start` - UNIX timestamp of the start time of the event
- `lead_samples` - number of samples saved before the first detected sample
- `start_threshold` - the value used to detect the start of the event
- `stop_threshold` - the value used to detect the end of the event
- `dead_time` - the dead time used to detect the end of the event in microseconds