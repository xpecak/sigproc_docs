# Sigproc user guide

Application for communication and setup of the hardware and for saving acquired measurement data.

![Sigproc GUI](img/sigproc.png)

## Basic Workflow

1. Connect to the hardware
2. Setup VGA
3. Setup graph settings
4. Setup file saving
5. Start measurement
6. Start saving data
7. Stop saving data
8. Stop measurement
9. Disconnect hardware

## Graph
The graph shows a quick view of the last received samples.
You can zoom the graph with the scroll wheel of the mouse, or pan it with left-click and drag.
If you pan or zoom on the axes, only the one axis will pan/zoom.

### Graph settings
There are two parameters to tweak for the graph, which affect performance of the graph.

__Downsample:__ The graph shows only every nth sample, skipping the previous n-1 samples.
Higher number increases the performance of the graph, but lowers the shown detail.
Recommended range is 64 to 4096.

__Count:__ Amount of data to show.
Arbitrary unit, does not directly mean number of data points shown.
Higher value lowers the performance of the graph, as it has to show more datapoints.
Recommende range is 1 to 10000.

__Warning!__ In general, doubling the downsample value means that you have to halve the count value to keep the same performance/refresh rate.
128 and 1000 is a good starting combination.


## Hardware setup
Before starting a measurement, you need to connect the the hardware and configure the VGA(Variable gain amplifier) on the board.

### Serial port
Configuration of the connection to the hardware.

__Connect to port:__ List of all the visible ports.  
__Refresh:__ Refreshes the list of visible ports.  
__Connect:__ Connect to the selected port.  
__Close:__ Disconnect from currently connected port.  

### VGA Configuration
Configures the internal variable gain amplifier in the hardware.
The new config values are not applied until you press the `Update` button.

__Pre-Amp Active:__ Whether the 30dB pre-amplifier is active or not.  
__Attenuation:__ Signal attenuation after the pre-amp.
Attenuation of -2dB is less than -20dB.
It is the amount of how much the signal amplitude is decreased.  
__Update:__ Uploads the selected configuration to the hardware.

__Warning!__ Currently the hardware doesn't work correctly when the Pre-Amp is active and/or the attenuation is lower than -2dB.

Depending on the measured signal, you might need to have a higher attenuation to fully utilize the dynamic range of the hardware.
You need to experiment a bit to find the correct settings for each signal.

## Measuring
There are 2 main modes for measuring: continuous and discontinuous.
The modes can run together.
Before starting a measurement, you need to be connected to a device.
Each mode can save the data to the output file by checking the __Save data__ checkbox in the according control section.

### Acquisition
This controls whether the instrument is measuring or not.
There is also an option to save the raw incoming data into the output file.

__Warning!__ Saving raw data produces large output files - easily several gigabytes.

### Continuous measurement
In this mode, the measured data is split into larger time chunks and various quantities are measured on this chunk.
Currently measured quantities are:
- Root-mean-square value over the time chunk
- Count of "swings" above two threshold values

There are several parameters to configure:
- __Count time:__ The duration of a single time chunk - between 1 ms and 2000 ms
- __Count1:__ First threshold value
- __Count2:__ Second threshold value

The buttons __Start__ and __Stop__ start and stop this mode.
The parameters are only updated after the measurement is restarted - they cannot be modified while the measurement is running.
The software also shows the last measured values when active.


### Discontinuous
In this mode, the software detects events using several parameters, which can be configured in the GUI.

- __Start threshold:__ Value over which the signal has to rise to detect an event
- __Stop threshold:__ Value under which the signal has to fall to end.
- __Dead time:__ For how long the signal has to stay under stop threshold to end.
- __Lead time:__ How many microseconds to save before the event start - between 0 and 5000 uS.

The buttons __Start__ and __Stop__ start and stop this mode.
The parameters are only updated after the measurement is restarted - they cannot be modified while the measurement is running.
The software also shows the total number of detected events since the last start of the measurement.


## File saving
You can select the location and name of the output file by clicking the __Browse...__ button.
The file is created when the first data is to be saved.