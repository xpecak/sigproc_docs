# Getting started

## Basic workflow

The basic workflow consists of using the hardware and the two applications (Sigproc and Sigprox Export).
You first measure the the data using the Sigproc app and the hardware.
After that, you can work directly with the HDF5 data file in a tool of your choice for analysis.
or use the Sigproc Export for basic analysis and/or exporting to a CSV file.

Detailed documentation for the Sigproc and Sigproc Export are in their respective chapters on left.

## Latest version download:

[Latest version download here](http://fi.muni.cz/~xpecak/Sigproc_sw.zip)

## Detailed example (with screenshots)

Will be added later.

## Developer contact info

Main mantainer: Oldrich Pecak - `oldrich.pecak@mail.muni.cz`