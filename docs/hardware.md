# Hardware
The hardware consists of a MCU with high speed 12bit ADC, high gain high precision amplifier circuit and required power circuitry.

## Connecting to the board

To use the board, you only need two steps:

1. Connect the sensor to the BNC connector.
2. Connect the board to a computer using the USB Type C connector.
Does not require USB 3.0, USB 2.0 is enough.
3. OPTIONAL! If you have modified the board to use external power supply, also connect the power supply to the barrel jack.

## Detailed description

![Amplifier render](img/amplifier_render.png)

__Controller__  
Microcontroller with external flash memory.
The high speed 12bit ADC is integrated into the microcontroller.
Controller acts as the bridge between USB and the analog data.

__Debug connectors__  
These connectors are used for debug and developement purposes and should not be needed during normal operation.
They include a JTAG header and a GPIO header.

__USB__  
There is a USB Type C connector for communication with the PC.
It is only wired for USB 2.0 speeds.
The circuit includes proper protection diodes on the connector.
The main board power is also took from the USB 5V bus, unless the 5V is supplied externally.
If the board is setup for external power supply, the 3v3 logic is still derived from USB, thus the main controller won't turn on unless you connect the USB cable.

__Power circuitry__  
The analog part requires low noise power supplies, both negative and positive rails.
The circuits for generating these symetric +/- 5V rails are in this part.
That includes the invering boost convertor and LDO regulators.

__Analog circuitry__  
Includes the amplifier cascade and required reference voltage generators.
During normal operation, the amplifiers are shielded by a aluminium can, to protect the signals from external noise sources.
The main amplifier is a variable gain amplifier.

__BNC Connector__  
Dedicated for connection of the sensor.
It is a standard BNC connector, impedance matched to 50 Ohms.

__Labels__  
Contains labels of all the test points on the board and pinouts of the debug connectors.

## Power
Usually, you can power the board via the USB-C connector.
If you wish to power it externally from separate source, you need the following modifications:

1. Populated J5 barrel jack connector
2. Disconnect the `V_USB V_ANALOG` jumper located next to the barrel jack
3. Provide 5V, 2A power to the barrel jack. Inner pin is positive, outer is negative.

The external power only powers the analog part (amplifiers and power suplies). The digital side of the board is always powered from USB 5V bus.

## Cooling
The analog amplifier part of the circuit dissipates up to 2.5 watts of thermal energy.
This is a substantial amount of heat and the board requires adequate cooling.
It is required to attach sizeable heatsink, using a suitable heat transfer medium (thermal paste, thermal glue) to the bottom part of the board, under the canned amplifier circuit.
There is an exposed copper pad on the bottom part, under the components which require the most cooling.
We recommend atleast a 85x45x25 mm heatsink with active cooling (a slow fan blowing across is enough).
A much larger heatsink is required to cool the board fully passively.

Board with correctly attached heatsink:  
![Board with heatsink](img/board_heatsink.jpg)